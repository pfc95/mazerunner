﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Personaje : NetworkBehaviour {
    int amount = 33;

    // Use this for initialization
    void Start () {
        
		
	}

    // Update is called once per frame
    void Update() {
        //movimientos del personaje
        if (!isLocalPlayer)

            return;
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            GetComponent<Rigidbody>().AddForce(new Vector3(-amount, 0, 0));        }

        if (Input.GetKey(KeyCode.RightArrow)) {
            GetComponent<Rigidbody>().AddForce(new Vector3(amount, 0, 0));        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, amount));        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, -amount));        }
        if (Input.GetKey(KeyCode.Space)) //saltar
        {
            GetComponent<Rigidbody>().AddForce(new Vector3(0, amount, 0));
        }
       



    }
}
